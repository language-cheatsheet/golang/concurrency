package concurrency

type WebsiteChecker func(string) bool

type result struct {
	string
	bool
}

func CheckWebsites(wc WebsiteChecker, urls []string) map[string]bool {
	results := make(map[string]bool)
	resultChannel := make(chan result)

	for _, url := range urls {
		// loop over each URL and start the request in a thread (goroutine)
		go func(u string) {
			// send the result to the channel
			resultChannel <- result{u, wc(u)}
		}(url)
	}

	// loop over all the urls and pull the result out of the channel one by one
	// this avoids race conditions with writing to the result map
	for i := 0; i < len(urls); i++ {
		// receive the result from the channel
		r := <-resultChannel
		results[r.string] = r.bool
	}

	return results
}

